create sequence attr_id_seq;

create table attribute
(
    id             text not null primary key default 'ATTR' || lpad(nextval('attr_id_seq'::regclass)::text, 7, '0'),
    attr_name      varchar(255) not null,
    data_type      varchar(50)  not null,
    is_array       boolean,
    attr_length    integer,
    attr_precision integer,
    attr_scale     integer
);

alter table attribute
    owner to postgres;