package com.rupaiya.rupaiyacore.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.rupaiya.rupaiyacore.validators.RNonZero
import com.rupaiya.rupaiyacore.validators.RNotNull
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.validation.constraints.NotNull
import kotlin.reflect.KClass

/**
 * Represents the Data Type for an attribute in Rupaiya. Multiple data types are supported in Rupaiya:
 *
 * - **String**, for descriptive attributes
 * - **Boolean**, for attributes requiring only a true/false/null value
 * - **Int**, for representing regular integers (E.g. 200)
 * - **Decimal**, for representing decimals (E.g. 24.552)
 * - **Date**, for attributes representing a date (E.g. 1st January 2020)
 * - **Time**, for attributes representing just a time value (e.g. 4:00 PM)
 * - **Datetime**, for attributes representing both a date and a time value (E.g. 1st January 2020, 4:00 PM)
 * - **Interval**, for attributes representing an interval in time (E.g. 5 minutes)
 * - **Entity**, for attributes looking up to another entity type
 *
 * Each datatype is represented as a subclass to RDataType class
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes(
        JsonSubTypes.Type(
                value = RString::class,
                name = "STRING"
        ),
        JsonSubTypes.Type(
                value = RBoolean::class,
                name = "BOOLEAN"
        ),
        JsonSubTypes.Type(
                value = RInt::class,
                name = "INT"
        ),
        JsonSubTypes.Type(
                value = RDecimal::class,
                name = "DECIMAL"
        ),
        JsonSubTypes.Type(
                value = RDate::class,
                name = "DATE"
        ),
        JsonSubTypes.Type(
                value = RTime::class,
                name = "TIME"
        ),
        JsonSubTypes.Type(
                value = RDateTime::class,
                name = "DATETIME"
        ),
        JsonSubTypes.Type(
                value = RInterval::class,
                name = "INTERVAL"
        )
)
sealed class RDataType(@RNotNull val type: DataType)

class RString(
    @RNonZero val length: Int
) : RDataType(DataType.STRING)

class RBoolean : RDataType(DataType.BOOLEAN)
class RInt : RDataType(DataType.INT)
class RDecimal(
        @RNonZero val precision: Int,
        @RNonZero val scale: Int
) : RDataType(DataType.DECIMAL)

class RDate : RDataType(DataType.DATE)
class RTime : RDataType(DataType.TIME)
class RDateTime : RDataType(DataType.DATETIME)
class RInterval : RDataType(DataType.INTERVAL)
class REntity(@RNotNull val entityType: String) : RDataType(DataType.ENTITY)

/**
 * Enum representing various datatypes possible in Rupaiya, and their mapping to Kotlin's classes
 */
enum class DataType(val kclass: KClass<*>) {
    STRING(String::class),
    BOOLEAN(Boolean::class),
    INT(Int::class),
    DECIMAL(Double::class),
    DATE(LocalDate::class),
    TIME(LocalTime::class),
    DATETIME(LocalDateTime::class),
    INTERVAL(Duration::class),
    ENTITY(String::class)
}
