package com.rupaiya.rupaiyacore.model

import com.rupaiya.rupaiyacore.dbspec.ATTR_DEFAULT
import com.rupaiya.rupaiyacore.validators.RNotNull
import javax.validation.Valid

data class RAttribute(
        val id: String = ATTR_DEFAULT,
        var name: String,
        val dataType: RDataType,
        var isArray: Boolean
)

data class RAttributeDTO(
        @RNotNull val name: String?,
        @field:Valid val dataType: RDataType?,
        @RNotNull val isArray: Boolean?
)
