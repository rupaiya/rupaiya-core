package com.rupaiya.rupaiyacore.api

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

object RResponse {

    fun <T> ok(ok: T) =
            ResponseEntity<T>(ok, HttpStatus.OK)

    fun <T> created(created: T) =
            ResponseEntity<T>(created, HttpStatus.CREATED)

    fun noContent() =
            ResponseEntity<Void>(HttpStatus.NO_CONTENT)
}
