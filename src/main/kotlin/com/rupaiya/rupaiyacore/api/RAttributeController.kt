package com.rupaiya.rupaiyacore.api

import com.rupaiya.rupaiyacore.model.RAttribute
import com.rupaiya.rupaiyacore.model.RAttributeDTO
import com.rupaiya.rupaiyacore.service.RAttributeService
import javax.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/attributes")
class RAttributeController(@Autowired val rAttributeService: RAttributeService) {

    @GetMapping
    fun getAllAttributes(): List<RAttribute> =
            rAttributeService.getAllAttributes()

    @GetMapping("/{attributeId}")
    fun getAttributeById(@PathVariable attributeId: String): RAttribute? =
            rAttributeService.getAttributeById(attributeId)

    @PostMapping
    fun insertAttribute(@Valid @RequestBody rAttributeDTO: RAttributeDTO): ResponseEntity<RAttribute> =
            RResponse.created(rAttributeService.createAttribute(rAttributeDTO))

    @PutMapping("/{attributeId}")
    fun updateAttribute(
        @PathVariable attributeId: String,
        @Valid @RequestBody rAttributeDTO: RAttributeDTO
    ): RAttribute? =
            rAttributeService.updateAttribute(attributeId, rAttributeDTO)

    @DeleteMapping("/{attributeId}")
    fun deleteAttribute(@PathVariable attributeId: String): ResponseEntity<Void> {
        rAttributeService.deleteAttribute(attributeId)
        return RResponse.noContent()
    }

}
