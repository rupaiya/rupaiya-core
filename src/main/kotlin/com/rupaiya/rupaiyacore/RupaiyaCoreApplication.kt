package com.rupaiya.rupaiyacore

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RupaiyaCoreApplication

fun main(args: Array<String>) {
    runApplication<RupaiyaCoreApplication>(*args)
}
