package com.rupaiya.rupaiyacore.dao

import com.rupaiya.rupaiyacore.dbspec.C_DATATYPE
import com.rupaiya.rupaiyacore.dbspec.C_ID
import com.rupaiya.rupaiyacore.dbspec.C_ISARRAY
import com.rupaiya.rupaiyacore.dbspec.C_LENGTH
import com.rupaiya.rupaiyacore.dbspec.C_NAME
import com.rupaiya.rupaiyacore.dbspec.C_PRECISION
import com.rupaiya.rupaiyacore.dbspec.C_SCALE
import com.rupaiya.rupaiyacore.dbspec.T_ATTRIBUTE
import com.rupaiya.rupaiyacore.exceptions.NoResultsFound
import com.rupaiya.rupaiyacore.model.DataType
import com.rupaiya.rupaiyacore.model.RAttribute
import com.rupaiya.rupaiyacore.model.RBoolean
import com.rupaiya.rupaiyacore.model.RDataType
import com.rupaiya.rupaiyacore.model.RDate
import com.rupaiya.rupaiyacore.model.RDateTime
import com.rupaiya.rupaiyacore.model.RDecimal
import com.rupaiya.rupaiyacore.model.RInt
import com.rupaiya.rupaiyacore.model.RInterval
import com.rupaiya.rupaiyacore.model.RString
import com.rupaiya.rupaiyacore.model.RTime
import java.sql.ResultSet
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.RecordMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

fun <T> T.returnNonNull(): T {
    if (this == null) throw NoResultsFound("No results were found from the database.")
    return this
}

fun <T> List<T>.returnNonNull(): List<T> {
    if (size < 1) throw NoResultsFound("No results were found from the database.")
    return this
}

@Repository
class RAttributeDao(
    @Autowired val dslContext: DSLContext
) {

    val rAttributeRecordMapper = RAttributeRecordMapper()

    //region Dao Methods
    fun getAllAttributes(): List<RAttribute> =
            dslContext.select()
                    .from(T_ATTRIBUTE)
                    .fetch(rAttributeRecordMapper)
                    .returnNonNull()

    fun getAttributeById(attributeId: String) =
            dslContext.selectFrom(T_ATTRIBUTE)
                    .where(C_ID.eq(attributeId))
                    .fetch(rAttributeRecordMapper)
                    .getOrNull(0)
                    .returnNonNull()

    fun insertAttribute(rAttribute: RAttribute): RAttribute {
        val result = dslContext
                .insertInto(T_ATTRIBUTE)
                .columns(
                        C_NAME,
                        C_DATATYPE,
                        C_ISARRAY,
                        C_LENGTH,
                        C_PRECISION,
                        C_SCALE
                )
                .values(
                        rAttribute.name,
                        rAttribute.dataType.type.toString(),
                        rAttribute.isArray,
                        if (rAttribute.dataType is RString) rAttribute.dataType.length else null,
                        if (rAttribute.dataType is RDecimal) rAttribute.dataType.precision else null,
                        if (rAttribute.dataType is RDecimal) rAttribute.dataType.scale else null
                )
                .returningResult(C_ID)
                .fetch()
                .getValue(0, C_ID) as String

        return rAttribute.copy(id = result)
    }

    fun updateAttribute(
        attributeId: String,
        rAttribute: RAttribute
    ): RAttribute? {
            dslContext
                    .update(T_ATTRIBUTE)
                    .set(
                            C_NAME,
                            rAttribute.name
                    )
                    .set(
                            C_ISARRAY,
                            rAttribute.isArray
                    )
                    .where(C_ID.eq(attributeId))
                    .execute()
        return getAttributeById(attributeId)
    }

    fun deleteAttribute(attributeId: String) =
            dslContext
                    .deleteFrom(T_ATTRIBUTE)
                    .where(C_ID.eq(attributeId))
                    .execute()

    //endregion

    //region RecordMapper
    class RAttributeRecordMapper : RecordMapper<Record, RAttribute> {
        override fun map(record: Record): RAttribute {
            return RAttribute(
                    id = record.get(C_ID) as String,
                    name = record.get(C_NAME) as String,
                    isArray = record.get(C_ISARRAY) as Boolean,
                    dataType = getDatatype(record)
            )
        }

        private fun getDatatype(record: Record): RDataType {
            return when (DataType.valueOf(record.get(C_DATATYPE) as String)) {
                DataType.STRING -> RString(record.get(C_LENGTH) as Int)
                DataType.DECIMAL -> RDecimal(
                        record.get(C_PRECISION) as Int,
                        record.get(C_SCALE) as Int
                )
                DataType.BOOLEAN -> RBoolean()
                DataType.INT -> RInt()
                DataType.DATE -> RDate()
                DataType.TIME -> RTime()
                DataType.DATETIME -> RDateTime()
                DataType.INTERVAL -> RInterval()
                DataType.ENTITY -> TODO()
            }
        }
    }
    //endregion
}
