package com.rupaiya.rupaiyacore.service

import com.rupaiya.rupaiyacore.dao.RAttributeDao
import com.rupaiya.rupaiyacore.model.RAttribute
import com.rupaiya.rupaiyacore.model.RAttributeDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RAttributeService(@Autowired val rAttributeDao: RAttributeDao) {

    fun getAllAttributes(): List<RAttribute> =
            rAttributeDao.getAllAttributes()

    fun getAttributeById(attributeId: String): RAttribute? =
            rAttributeDao.getAttributeById(attributeId)

    fun createAttribute(rAttributeDTO: RAttributeDTO): RAttribute {
        val rAttribute = RAttribute(
                name = rAttributeDTO.name!!,
                dataType = rAttributeDTO.dataType!!,
                isArray = rAttributeDTO.isArray!!
        )
        return rAttributeDao.insertAttribute(rAttribute)
    }

    fun updateAttribute(
        attributeId: String,
        rAttributeDTO: RAttributeDTO
    ): RAttribute? {
        val rAttribute = getAttributeById(attributeId)
        rAttributeDTO.name?.let { rAttribute?.name = it }
        rAttributeDTO.isArray?.let { rAttribute?.isArray = it }
        return rAttribute?.let {
            rAttributeDao.updateAttribute(attributeId, it)
        }
    }

    fun deleteAttribute(attributeId: String) =
            rAttributeDao.deleteAttribute(attributeId)
}
