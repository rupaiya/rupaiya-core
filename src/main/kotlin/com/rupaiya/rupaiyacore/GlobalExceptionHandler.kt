package com.rupaiya.rupaiyacore

import com.rupaiya.rupaiyacore.exceptions.InvalidRequest
import com.rupaiya.rupaiyacore.exceptions.NoResultsFound
import com.rupaiya.rupaiyacore.exceptions.RExceptionResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@EnableWebMvc
@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(value = [NoResultsFound::class])
    fun notFound(e: NoResultsFound): ResponseEntity<RExceptionResponse> =
            prepareResponseEntity(
                    e.message,
                    HttpStatus.NOT_FOUND
            )

    @ExceptionHandler(value = [InvalidRequest::class])
    fun badRequest(e: InvalidRequest): ResponseEntity<RExceptionResponse> =
            prepareResponseEntity(
                    e.message,
                    HttpStatus.BAD_REQUEST
            )

    @ExceptionHandler(value = [MethodArgumentNotValidException::class])
    fun constraintViolation(e: MethodArgumentNotValidException): ResponseEntity<RExceptionResponse> =
            prepareResponseEntityWithErrors(
                    e,
                    HttpStatus.BAD_REQUEST
            )

    @ExceptionHandler(value = [Exception::class, RuntimeException::class])
    fun internalServerError(e: Exception): ResponseEntity<RExceptionResponse> =
            prepareResponseEntity(
                    "Unknown error occurred",
                    HttpStatus.INTERNAL_SERVER_ERROR
            )

    private fun prepareResponseEntity(
        message: String?,
        status: HttpStatus
    ): ResponseEntity<RExceptionResponse> =
            ResponseEntity<RExceptionResponse>(
                    RExceptionResponse(
                            message,
                            null,
                            status.value()
                    ),
                    status
            )

    private fun prepareResponseEntityWithErrors(
        ex: MethodArgumentNotValidException,
        status: HttpStatus
    ): ResponseEntity<RExceptionResponse> {
        val errors = arrayListOf<String>()
        ex.bindingResult.fieldErrors.forEach { errors.add("${it.field} : ${it.defaultMessage}") }
        return ResponseEntity<RExceptionResponse>(
                RExceptionResponse(
                        "Validation errors found",
                        errors,
                        status.value()
                ),
                status
        )
    }
}
