package com.rupaiya.rupaiyacore.dbspec

import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table

const val D_RUPAIYA = "rupaiya"

val T_ATTRIBUTE = table("attribute")

val C_ID = field("id", String::class.java)
val C_NAME = field("attr_name", String::class.java)
val C_DATATYPE = field("data_type", String::class.java)
val C_ISARRAY = field("is_array", Boolean::class.java)
val C_LENGTH = field("attr_length", Int::class.java)
val C_PRECISION = field("attr_precision", Int::class.java)
val C_SCALE = field("attr_scale", Int::class.java)
val ATTR_DEFAULT = "ATTR"
