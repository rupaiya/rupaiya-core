package com.rupaiya.rupaiyacore.exceptions

class NoResultsFound(message: String) : RuntimeException(message)

class InvalidRequest(message: String) : RuntimeException(message)

data class RExceptionResponse(
    val message: String?,
    val errors: List<String>?,
    val statusCode: Int
)
