package com.rupaiya.rupaiyacore.validators

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

/**
 * Custom validation annotation to check for non-null fields. Required because
 * of https://gitlab.com/rupaiya/rupaiya-core/-/issues/2
 */
@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [RNotNullValidator::class])
annotation class RNotNull(
    val message: String = "{javax.validation.constraints.NotNull.message}",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class RNotNullValidator : ConstraintValidator<RNotNull, Any> {
    override fun isValid(value: Any?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) return false
        return true
    }
}

@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [RNonZeroValidator::class])
annotation class RNonZero(
        val message: String = "must not be 0",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = []
)

class RNonZeroValidator : ConstraintValidator<RNonZero, Int> {
    override fun isValid(value: Int?, context: ConstraintValidatorContext?): Boolean {
        if (value == null || value == 0) return false
        return true
    }
}

